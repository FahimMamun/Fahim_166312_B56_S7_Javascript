Command line instructions


Git global setup

git config --global user.name "Fahim Al Mamun"
git config --global user.email "alfahimmamun@gmail.com"

Create a new repository

git clone https://FahimMamun@gitlab.com/FahimMamun/Fahim_166312_B56_S7_Javascript.git
cd Fahim_166312_B56_S7_Javascript
touch README.md
git add README.md
git commit -m "add README"
git push -u origin master

Existing folder

cd existing_folder
git init
git remote add origin https://FahimMamun@gitlab.com/FahimMamun/Fahim_166312_B56_S7_Javascript.git
git add .
git commit
git push -u origin master

Existing Git repository

cd existing_repo
git remote add origin https://FahimMamun@gitlab.com/FahimMamun/Fahim_166312_B56_S7_Javascript.git
git push -u origin --all
git push -u origin --tags